ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/alplain
ARG FROM_IMG_NAME=openjdk-jre-headless
ARG FROM_IMG_TAG="2020-04-13-3"

FROM ${FROM_IMG_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}

ARG GOCD_URL=https://download.gocd.io/binaries
ARG GOCD_VER=22.2.0
ARG GOCD_SUBVER=14697

ENV GO_SERVER_URL=https://tasks.server:8154/go \
    GOCD_LOCAL_DOCKERENGINE=false \
    GOCD_CLEAN_IMAGES=false \
    DOCKER_TAG_REV=true \
    GOCD_AGENT_AUTOENABLE_KEY=qnibFTW \
    GOCD_AGENT_AUTOENABLE_ENV=latest,upstream,docker,deploy \
    GOCD_AGENT_AUTOENABLE_RESOURCES=alpine \
    DOCKER_REPO_DEFAULT=qnib \
    GOPATH=/usr/local/ \
    DOCKER_CONSUL_DNS=false \
    ENTRYPOINTS_DIR=/opt/qnib/entry/ \
    ENTRY_USER=gocd \
    LANG=en_US.utf8 \
    HOME_DIR=/home

# allow mounting ssh keys, dotfiles, and the go server config and data
VOLUME /godata


RUN apk add --no-cache wget git jq perl sed bc curl go gcc openssl make file py-pip rsync docker python3-dev libc-dev libffi-dev libarchive-tools \
 && pip install docker-compose \
 && rm -rf /var/cache/apk/* /tmp/* /opt/go-agent/config/autoregister.properties \
 && adduser -s /sbin/nologin -u 5000 -D -H -h /opt/go-agent/ gocd
## Allow for reusable file-system layers
RUN mkdir -p /opt/go-agent/ \
 && echo "Download '${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-agent-${GOCD_VER}-${GOCD_SUBVER}.zip'" \
 && wget -qO - ${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-agent-${GOCD_VER}-${GOCD_SUBVER}.zip |bsdtar xfz - -C /opt/go-agent/ --strip-components=1 \
 && chmod +x /opt/go-agent/bin/go-agent
#RUN wget -qO /usr/local/bin/go-dckrimg $(/usr/local/bin/go-github rLatestUrl --ghrepo go-dckrimg --regex ".*inux") \
# && chmod +x /usr/local/bin/go-dckrimg
RUN wget -qO - $(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo service-scripts --regex ".*.tar" --limit 1) |tar xf - -C /opt/
ADD opt/qnib/gocd/agent/bin/check.sh /opt/qnib/gocd/agent/bin/
ADD opt/qnib/gocd/agent/bin/start.sh /opt/qnib/gocd/agent/bin/
COPY opt/qnib/entry/20-gocd-render-autoregister-conf.sh \
     opt/qnib/entry/30-chown-gocd-files.sh \
     opt/qnib/entry/40-unpack-bundles.sh \
     /opt/qnib/entry/
COPY opt/qnib/gocd/etc/autoregister.properties /opt/qnib/gocd/etc/
CMD ["/opt/qnib/gocd/agent/bin/start.sh"]
